import Vue from 'vue'
import Element from 'element-ui'
import './../assets/styles/vendors/element-variables.scss'
import locale from 'element-ui/lib/locale/lang/fr'

Vue.use(Element, { locale })
