/* eslint-disable no-unused-vars */
import { BrowserWindow, screen } from 'electron'
const path = require('path')

const createWindow = () => {
  let browserWindow = new BrowserWindow({
    /// Définir la largeur et la hauteur de la fenêtre
    width: 400,
    height: 400,
    /// Retirez la fenêtre de la fenêtre, de sorte qu'il deviendra une fenêtre sans cadre
    frame: false,
    /// et définir la transparence pour supprimer toute couleur de fond de la fenêtre
    transparent: true,
    resizable: false,
    show: false,
    icon: path.join(__dirname, '.icon-set/icon_256x256.png')
  })

  console.log(__dirname)
  browserWindow.loadURL(`file://${__dirname}/splash.html`)

  if (!process.env.IS_TEST) {
    browserWindow.webContents.openDevTools({ mode: 'detach' })
  }

  // Trouver l'affichage où le curseur de la souris sera
  const cursorScreenPoint = screen.getCursorScreenPoint()
  const currentDisplay = screen.getDisplayNearestPoint(cursorScreenPoint)
  // Définir la position de la fenêtre sur cette affichage des coordonnées
  browserWindow.setPosition(currentDisplay.workArea.x, currentDisplay.workArea.y)
  browserWindow.center()

  browserWindow.on('closed', () => (browserWindow = null))
  browserWindow.webContents.on('ready-to-show', () => {
    browserWindow.show()
  })

  return browserWindow
}

export default createWindow()
