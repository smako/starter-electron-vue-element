/* eslint-disable no-undef */
'use strict'

import { app, protocol, BrowserWindow } from 'electron'
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer'
const isDevelopment = process.env.NODE_ENV !== 'production'

// Schéma doit être enregistré avant que l'application soit prête
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])

async function createWindow() {
  const SplashWindow = require('./SplashWindow').default
  const MainWindow = require('./MainWindow').default

  /// Continuez à écouter l'événement, lorsque la contenance MainWindow a chargé
  MainWindow.webContents.on('did-finish-load', () => {
    setTimeout(() => {
      /// puis fermez la fenêtre d'écran de chargement et affichez la fenêtre principale.
      if (SplashWindow) {
        SplashWindow.close()
      }
      MainWindow.show()
    }, 3000)
  })
}

// Quittez quand toutes les fenêtres sont fermées.
app.on('window-all-closed', () => {
  // Sur MacOS, il est courant pour les applications et leur barre de menus
  // Pour rester actif jusqu'à ce que l'utilisateur quitte explicitement avec cmd + q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // Sur MacOS, il est commun de ré-créer une fenêtre dans l'application lorsque le
  // L'icône de quai est cliquée et il n'y a pas d'autres fenêtres ouvertes.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// Cette méthode sera appelée lorsque Electron a fini l'initialisation et prêt à créer des fenêtres de navigateur.
// Certaines API ne peuvent être utilisées que lorsque cet événement se produit.
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Installez Vue DevTools
    try {
      await installExtension(VUEJS_DEVTOOLS)
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
})

// Sortez proprement sur demande du processus parent en mode développement.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', data => {
      if (data === 'graceful-exit') {
        app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      app.quit()
    })
  }
}
