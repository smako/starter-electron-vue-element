import { BrowserWindow, screen } from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
const path = require('path')

const createWindow = () => {
  const browserWindow = new BrowserWindow({
    titleBarStyle: 'hidden',
    width: 1281,
    height: 800,
    minWidth: 1281,
    minHeight: 800,
    useContentSize: true,
    webPreferences: {
      // Utilisez pluginoption.nodeIngration, laissez cela seul
      // Voir nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-Integration pour plus d'informations
      nodeIntegration: false,
      contextIsolation: true
    },
    transparent: true,
    show: false,
    icon: path.join(__dirname, '.icon-set/icon_256x256.png')
  })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Chargez l'URL du serveur DEV si en mode développement
    browserWindow.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) {
      browserWindow.webContents.openDevTools({ mode: 'bottom' })
    }
  } else {
    createProtocol('app')
    // Charger l'index.html lorsqu'il n'est pas en développement
    browserWindow.loadURL('app://index.html')
  }

  // Trouver l'affichage où le curseur de la souris sera
  const cursorScreenPoint = screen.getCursorScreenPoint()
  const currentDisplay = screen.getDisplayNearestPoint(cursorScreenPoint)
  // Définir la position de la fenêtre sur cette affichage des coordonnées
  browserWindow.setPosition(currentDisplay.workArea.x, currentDisplay.workArea.y)
  browserWindow.center()

  return browserWindow
}

export default createWindow()
