module.exports = {
  configureWebpack: {
    devtool: 'source-map'
  },
  pluginOptions: {
    electronBuilder: {
      mainProcessFile: 'src/main/index.js',
      rendererProcessFile: 'src/renderer/main.js',
      builderOptions: require('./builder.config')
    }
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: '@import "@/renderer/assets/styles/abstracts/_variables.scss";'
      }
    }
  }
}
