// https://www.electron.build/configuration

const ICONS_DIR = 'src/renderer/assets/icons'

const windowsOS = {
  win: {
    icon: ICONS_DIR + '/icon.ico',
    publisherName: '',
    target: 'nsis'
  },

  nsis: {
    differentialPackage: true
  }
}

const linuxOS = {
  linux: {
    icon: ICONS_DIR,
    target: ['AppImage' /*, 'deb' */],
    category: 'Utility',
    maintainer: ''
  }
}

const macOS = {
  mac: {
    target: 'dmg',
    icon: ICONS_DIR + '/icon.icns'
  },
  dmg: {
    contents: [
      {
        x: 410,
        y: 150,
        type: 'link',
        path: '/Applications'
      },
      {
        x: 130,
        y: 150,
        type: 'file'
      }
    ]
  }
}

module.exports = {
  asar: false,
  productName: '',
  appId: 'org.compagny.name',
  artifactName: '${name}-${version}.${ext}',
  files: [
    {
      from: 'src/resources',
      to: 'dist_electron/resources'
    }
  ],
  ...windowsOS,
  ...linuxOS,
  ...macOS
}
